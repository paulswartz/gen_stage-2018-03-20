# GenStage

<div>march 20, 2018</div>
<div>boston elixir @ MBTA</div>

Note: thanks everyone for coming! I'm Paul Swartz, lead architect here at MBTA, and tonight I'll be talking about GenStage.

---
> "a specification for exchanging events between producers and consumers."

Note: The README describes it as .... But that doesn't showcase what's
interesting about the library.

---
#### `everyone |> loves |> pipelines!`

Note: almost everyone coming to Elixir raves about the pipeline
operator. but, it's syntactic sugar for calling functions; it doesn't take
advantage of OTP's concurrency or supervision

---
# Stages

Note: GenStage introduces the concept of stages. Conceptually, they are the
functions in our pipeline operator example, but allow for concurrency and
supervision. There are three (3) types.

---
## Producer

* returns a given amount of data when requested
* implements the `handle_demand/2` callback

---
## Consumer

* does something with incoming data
* implements the `handle_events/3` callback

---
## Producer Consumer

* combination of Producer and Consumer
* work as a filter/map stage
* implements `handle_event/3`

---
# Examples

Note: let's look at some small examples from the documentation

---
## Producer
<pre><code class="elixir" data-trim>
defmodule A do
  use GenStage

  def init(counter) do
    {:producer, counter}
  end

  def handle_demand(demand, counter) when demand > 0 do
    events = Enum.to_list(counter..counter+demand-1)
    {:noreply, events, counter + demand}
  end
end

{:ok, a} = GenStage.start_link(A, 0)
</code></pre>

Note: If the counter is 3 and we ask for 2 items, we wil emit the items 3 and
4, and set the state to 5. `init/1` returns the stage type and the initial state.

---
## Producer Consumer

<pre><code class="elixir" data-trim>
defmodule B do
  use GenStage

  def init(number) do
    {:producer_consumer, number}
  end

  def handle_events(events, _from, number) do
    events = for event <- events, do: event * number
    {:noreply, events, number}
  end
end

{:ok, b} = GenStage.start_link(B, 2)
</code></pre>

Note: multiplies each event by the given number. It can also return more or
fewer events than it receives.

---
## Consumer
<pre><code class="elixir" data-trim>
defmodule C do
  use GenStage

  def init(:ok) do
    {:consumer, :the_state_does_not_matter}
  end

  def handle_events(events, _from, state) do
    Process.sleep(1000)
    IO.inspect(events)
    {:noreply, [], state}
  end
end

{:ok, c} = GenStage.start_link(C, :ok)
</code></pre>

Note: Sleeps for 1 second, then prints the events it received. Since it's a
consumer, we would never emit items: the second item of the tuple is always
the empty list. Now all that's left is to hook them together.

---
# Subscribing

connecting the stages

---
`sync_subscribe/2`
<pre><code class="elixir" data-trim>
GenStage.sync_subscribe(b, to: a)
GenStage.sync_subscribe(c, to: b)
</code></pre>

Note: there's also `async_subscribe`, but that's more for internal use.

---
better: subscribe from `init/1`
<pre><code class="elixir" data-trim>
defmodule C
  def init(producers)
    {:consumer, :does_not_matter, subscribe_to: producers}
  end
end

{:ok, _} = GenStage.start_link(B, 2, name: B)
{:ok, _} = GenStage.start_link(C, [B])
</code></pre>

Note: it's better because if the consumer crashes, it will automatically
re-subscribe when the supervisor restarts it

---
shell

---
<pre><code class="elixir" data-trim>
[0, 2, 4, 6, 8, 10, ...]
[1000, 1002, 1004, 1006, 1008, 1010, ...]
</code></pre>
why 500 events?

---
# Demand

consumers tell producers how much data to send

Note: Demand flows from consumers to producers, as a form of
back-pressure. if consumers slow down, producers will run out of available
demand and will stop sending events until the consumers catch
up. producers/producers can also buffer (10k items for producers, infinity
for producerconsumers). can keep either first or last items.

---
# Dispatchers

pipelines with joints

Note: module which describes how a producer sends events to subscribe
consumers

---
<div>`DemandDisptcher`</div>
<div>sends to consumer with the highest demand <small>(default)</small></div>
<div class="fragment">
    <div>`BroadcastDispatcher`</div>
    <div>sends events to all consumers when they have demand (fan-out)</div>
<div class="fragment">
    <div>`PartitionDispatcher`</div>
    <div>sends to a particular consumer based on a hash function</div>
</div>

---
# ConsumerSupervisor

taking advantage of concurrency

---
<pre><code class="elixir" data-trim>
defmodule Consumer do
  use ConsumerSupervisor

  def start_link() do
    ConsumerSupervisor.start_link(__MODULE__, :ok)
  end

  def init(:ok) do
    children = [worker(Printer, [], restart: :temporary)]

    {:ok, children,
     strategy: :one_for_one,
     subscribe_to: [A]}
  end
end
</code></pre>

Note: This calls `Printer.start_link(event)` for each event received. It's
similar to the `:simple_one_for_one` supervision strategy, or the
`DynamicSupervisor` behavior.

---
# Concentrate

Note: Let's look at an example architecture which uses GenStage. Concentrate
is an application the MBTA uses to combine realtime information from across
our systems.

---
<!-- .slide: data-background-image="/img/concentrate.svg" data-background-size="contain" -->

Note: sources (producers) get data by making HTTP requests and
parsing. MergeFilter receives events from all the sources, then broadcasts to
Reporters (statistics) and Encoders (output data). All the encoders pass
their encoded data to a ConsumerSupervisor which spawns a Sink to upload the
data.

---
# Tips & Tricks

Note: I'll finish up with some tips I've picked up from using GenStage for
projects here @ MBTA.

---
## Stages for Concurrency

modules/functions for logical separation

Note: The best use of stages is for places where you want concurrency. Don't
use stages just because the data is logically flowing between different parts
of your system: that's better represented by having a single stage use
different modules internally. If we look back at the Concentrate architecture...

---
<!-- .slide: data-background-image="/img/concentrate.svg" data-background-size="contain" -->

Note: ...we can see MergeFilter is doing a lot
of work: merging the different sources together, filtering that data, and
grouping it, before passing it along. Those could be implemented as different
stages, but since there's only the single path through them, it's only added
complexity.

---
## Demand management

try changing min/max_demand

Note: each subscription has a miniumum and maximum amount of outstanding
demand. Tuning these can avoid data building up between stages, and keep data flowing smoothly.

---
## Supervision

you probably want the `:rest_for_one`

Note: if one a consumer crashes, it'll generally crash any producers, and
possibly crashing the supervisor as well. `:rest_for_one` restarts the whole
tree at once.

---
## Testing
* `GenStage.from_enumerable/2`
* `GenStage.stream/2 |> Enum.take/2`

Note: GenStage comes with two helpful functions for testing your
stages. `from_enumerable/2` makes a producer stage from an
enumerable. `stream/2` makes an Enumerable by subscribing to a producer
stage. And at the end of the day, you can always call `handle_demand/2` and
`handle_events/3` manually!

---
# Further references

* `gen_stage` (hex)
* `flow` (hex)
* [José's keynote](https://youtu.be/srtMWzyqdp8?t=244)

Note: Flow provides an Enumerable-like API on top of GenStage

---
# We're hiring!

[https://jobs.lever.co/mbta](https://jobs.lever.co/mbta)

Note: hiring for engineers, product and project managers, designers

---
# Thanks!

* @paulswartz
* paul@paulswartz.net
* [paulswartz.net](https://paulswartz.net)
